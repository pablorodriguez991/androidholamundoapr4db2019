package test.inacap.holamundoapr4db2019;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Declarar componentes globales
    private EditText etNombre, etPass;
    private Button btIngresar;
    private TextView tvCrearCuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.etNombre = findViewById(R.id.etNombre);
        this.etPass = findViewById(R.id.etPassword);
        this.btIngresar = findViewById(R.id.btIngresar);
        this.tvCrearCuenta = findViewById(R.id.tvCrearCuenta);

        // Detectar click sobre un boton
        this.btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Obtener datos ingresados por el usuario
                String nombre = etNombre.getText().toString();

                // Mostrar los datos
                // Primera, mostrar datos al usuario, estatico
                tvCrearCuenta.setText("Prueba: " + nombre);

                // Segunda, mostrar datos al usuario, temporal
                Toast.makeText(getApplication(), "Su nombre es " + nombre, Toast.LENGTH_SHORT).show();

                // Tercera, mostrar datos al programador (Logcat)
                Log.i("PRUEBA", "El nombre del usuario es " + nombre);
            }
        });

    }
}























